import ssl

from irc import bot, connection

import config
from user import User


def real_nick(nick):
    '''Strip IRC decorator symbols from ``nick``.'''
    if not nick:
        return ''
    return nick.strip('+%@&~')


class JuizBot(bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port):
        # Handle SSL connection
        conn_wrapper = connection.identity
        if config.ssl:
            conn_wrapper = ssl.wrap_socket

        super().__init__(
            [(server, port)],
            nickname,
            nickname,
            connect_factory=connection.Factory(wrapper=conn_wrapper))

        # Bot custom attributes
        self.authenticated_users = {}

    def on_join(self, c, e):
        '''Whenever a user joins a chan, add them to the dictionary'''
        nick = real_nick(e.source.nick)
        if not nick in self.authenticated_users:
            self.authenticated_users[nick] = User(self, nick)
            self.authenticated_users[nick].authenticate()

    def on_welcome(self, c, e):
        '''When the JuizBot is connected to the server'''
        c.join(config.channel)
        '''307 is the code associated to nickserv response events
        http://www.mirc.com/rfc2812.html'''
        c.add_global_handler("307", self.on_nickserv_response)
        '''353 is the code associated to the response to NAMES command,
        which is sent whenever you join a chan'''
        c.add_global_handler("353", self.on_namreply)
        #c.add_global_handler("all_events", self.debug)

    def on_part(self, c, e):
        self.remove_user(real_nick(e.source.nick))

    def on_kick(self, c, e):
        self.remove_user(real_nick(e.source.nick))

    def on_quit(self, c, e):
        self.remove_user(real_nick(e.source.nick))

    def debug(self, c, e):
        print(vars(e))

    def on_privnotice(self, c, e):
        pass

    def on_privmsg(self, c, e):
        pass

    def on_pubmsg(self, c, e):
        '''When receiving a message on a chan'''
        self.actions(e.arguments[0], real_nick(e.source.nick))

    def on_namreply(self, c, e):
        '''When receiving a list of NAMES, add them to the dictionary
        and try to authenticate them'''
        for nick in e.arguments[2].split():
            nick = real_nick(nick)
            if not nick in self.authenticated_users:
                self.authenticated_users[nick] = User(self, nick)
            if nick in self.authenticated_users:
                self.authenticated_users[nick].authenticate()

    def on_nickserv_response(self, c, e):
        '''Catches nickserv's response'''
        nick = e.arguments[0]
        registered = e.arguments[1] == 'is identified for this nick'
        self.authenticated_users[nick].authenticate_response(registered)

    def actions(self, msg, nick):
        '''A list of actions depending on the message received'''
        if msg.split()[0] == "!auth":
            if not nick in self.authenticated_users:
                self.authenticated_users[nick] = User(self, nick)
            self.authenticated_users[nick].authenticate(True)
        else:
            self.connection.privmsg(config.channel, self.authenticated_users)

    def remove_user(self, nick):
        del self.authenticated_users[nick]


def main():
    import sys
    import config
    bot = JuizBot(config.channel, config.nickname, config.server, config.port)
    bot.start()


if __name__ == "__main__":
    main()
