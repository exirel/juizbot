# JuizBot #
An IRC bot with a memo service.

### What does she do? ###
Right now, she just joins chans and check if users are authenticated to nickserv.

A user can ask the bot to check them again with !auth.

### How do I get set up? ###
Get python 3

pip install irc

Edit the configuration file

./juizbot.py

### Contribution guidelines ###
Don't actively contribute for now. I learn Python with this project.

But if you want to give me pointers, feel free to mail me at firstname.lastname@gmail.com.
My first name is Erwan.

### Special thanks ###
To Exirel for giving me pointers.

\#dev from irc.nobleme.com in general