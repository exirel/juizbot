class User:

    def __init__(self, bot, nick):
        self.bot = bot
        self.nick = nick.strip('+%@&~')
        self.authenticated = False
        self.user_asked_authentication = False

    def __str__(self):
        return self.nick + (' is authenticated' if self.authenticated else '')

    def __repr__(self):
        return self.__str__()

    def authenticate(self, user_asked=False):
        '''Send a WHOIS command, and store if the user explicitly asked it.
        If they didn't, don't notify them'''
        if self.authenticated == False:
            self.bot.connection.whois(self.nick)
            self.user_asked_authentication = user_asked
        else:
            self.bot.connection.notice(self.nick,
                    "You're already authenticated.")

    def authenticate_response(self, response):
        '''Upon nickserv answer, check if the user is authenticated. If the
        user explicitly asked for an authentication, notify them'''
        if response:
            if self.authenticated == False and self.user_asked_authentication:
                self.bot.connection.notice(self.nick,
                        "You're now authenticated.")
            self.authenticated = True
        else:
            if self.user_asked_authentication:
                self.bot.connection.notice(self.nick,
                        "You're not authenticated. " +
                        "Try again when you're authenticated with nickserv")
